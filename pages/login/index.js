import { useState, useContext } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap'
import { GoogleLogin } from 'react-google-login'
import Router from 'next/router'
import Swal from 'sweetalert2'

import UserContext from '../../UserContext';
import View from '../../components/View'

export default function index() {
    return (
        <View title={ 'Login' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3 className="text-center">Login</h3>
                    <LoginForm/>
                </Col>
            </Row>
        </View>
    )
}


const LoginForm = () => {
    const { user, setUser } = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    const [tokenId, setTokenId] = useState(null)

    const authenticate = (e) => {
        e.preventDefault()

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }

        fetch('https://csp3-dupaya.herokuapp.com/api/users/login', options)
        .then(res => res.json())
        .then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'does-not-exist') {
                    Swal.fire('Authentication Failed', 'User does not exists', 'error')
                } else if (data.error === 'incorrect-password') {
                    Swal.fire('Authentication Failed', 'Password is incorrect', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'Try alternative login', 'error')
                }
            }
        })
    }

    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: {
                Authorization: `Bearer ${ accessToken }`
            }
        }

        fetch('https://csp3-dupaya.herokuapp.com/api/users/user-details', options)
        .then(res => res.json())
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })
            Router.push('/tracker')
        })
    }

    const authenticateGoogleToken = (response) => {
        setTokenId(response.tokenId)
        console.log(response)
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                tokenId: response.tokenId
            })
        }

        fetch('https://csp3-dupaya.herokuapp.com/api/users/login-google', payload)
        .then(res => res.json())
        .then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'google-auth-error') {
                    Swal.fire('Google Auth Error', 'Google Authentication failed', 'error')
                } else {
                    Swal.fire('Login Type Error', 'No google account found', 'error')
                }
            }
        })
    }

    return (
        <Card>
            <Card.Header>Login Details</Card.Header>
            <Card.Body>
                <Form onSubmit={ authenticate }>
                    <Form.Group controlId="email">
                        <Form.Label>Email</Form.Label>
                        <Form.Control type="email" value={ email } onChange={ (e) => setEmail(e.target.value) } autoComplete="off" required/>
                    </Form.Group>
                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={ password } onChange={ (e) => setPassword(e.target.value) } required/>
                    </Form.Group>
                    <Button className="mb-2" variant="success" type="submit" block>Login</Button>
                    <GoogleLogin
                        clientId="626126006176-mrh7gvkr4624mfisq6fec6cjjr9eu2ar.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={ authenticateGoogleToken }
                        onFailure={ authenticateGoogleToken }
                        cookiePolicy={ 'single_host_origin' }
                        className="w-100 text-center d-flex justify-content-center"
                    />
                </Form>
            </Card.Body>
        </Card>
    )
}