import { useState, useEffect, useContext } from 'react'
import { Row, Col, Container, Tabs, Tab } from 'react-bootstrap'
import UserContext from '../../UserContext'
import moment from 'moment'
import View from '../../components/View'
//Bar Charts
import IncomeBarChart from '../../components/IncomeBarChart'
import ExpensesBarChart from '../../components/ExpensesBarChart'
//Pie Charts
import IncomePieChart from '../../components/IncomePieChart'
import ExpensesPieChart from '../../components/ExpensesPieChart'
//Line Chart
import IncomeLineChart from '../../components/IncomeLineChart'
import ExpensesLineChart from '../../components/ExpensesLineChart'

export default function index() {
	const { user } = useContext(UserContext)

	return (
		<View title="Stats">
			<Container>
			<Tabs transition={false} id="barchart-tab">
				<Tab eventKey="barchart" title="Bar Chart">
					<Row>
						<Col xs={12} md={6}>
							<IncomeBarChart />
						</Col>

						<Col xs={12} md={6}>
							<ExpensesBarChart />
						</Col>
					</Row>
				</Tab>

				<Tab eventKey="piechart" title="Pie Chart">
					<Row>
						<Col xs={12} md={6}>
							<IncomePieChart />
						</Col>

						<Col xs={12} md={6}>
							<ExpensesPieChart />
						</Col>
					</Row>
				</Tab>

				<Tab eventKey="linechart" title="Line Chart">
					<Row>
						<Col xs={12} md={6}>
							<IncomeLineChart />
						</Col>

						<Col xs={12} md={6}>
							<ExpensesLineChart />
						</Col>
					</Row>
				</Tab>
			</Tabs>
			</Container>
		</View>
	)
}