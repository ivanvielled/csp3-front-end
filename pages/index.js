import Head from 'next/head'
import { Container } from 'react-bootstrap'

export default function Home() {

  return (
   <React.Fragment>
      <Head>
        <title>Swak - Budget Tracker</title>
      </Head>
    </React.Fragment>
  )
}
