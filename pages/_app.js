import { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles.css'
import { Container } from 'react-bootstrap'
import { UserProvider } from '../UserContext'
import NavBar from '../components/NavBar'

function MyApp({ Component, pageProps }) {

	const [user, setUser] = useState({
		id: null,
		email: null,
		isAdmin: null
	})

	useEffect(() => {
		const accessToken = localStorage.getItem('token')
		const options = {
			headers: {
				Authorization: `Bearer ${accessToken}`
			}
		}

		fetch('https://csp3-dupaya.herokuapp.com/api/users/user-details', options)
		.then(res => res.json())
		.then(data => {
			setUser({
				id: data._id,
				email: data.email,
				isAdmin: data.isAdmin
			})
		})
	}, [user.id])

	const unsetUser = () => {
		localStorage.clear()

		setUser({
			id: null,
			email: null,
			isAdmin: null
		});
	}
  	return (
	  	<UserProvider value={{user, setUser, unsetUser}}>
	  		<NavBar />
	  		<Component {...pageProps} />
	  	</UserProvider>
	)
}

export default MyApp