import { useState, useEffect, useContext } from 'react'
import { Form, Row, Col, Container, Button, Tabs, Tab, Card } from 'react-bootstrap'
import UserContext from '../../UserContext'
import moment from 'moment'
import View from '../../components/View'

export default function index() {
	const { user } = useContext(UserContext)
	// console.log(user)
	//income
	const [incomeSource, setIncomeSource] = useState('')
	const [incomeDesc, setIncomeDesc] = useState('')
	const [incomeAmount, setIncomeAmount] = useState(0)
	const [totalIncome, setTotalIncome] = useState(0)
	const [incomeSave, setIncomeSave] = useState('')
	const [incomeRecord, setIncomeRecord] = useState([])

	//expenses
	const [expensesName, setExpensesName] = useState('')
	const [expensesDesc, setExpensesDesc] = useState('')
	const [expensesAmount, setExpensesAmount] = useState(0)
	const [totalExpenses, setTotalExpenses] = useState(0)
	const [expensesSave, setExpensesSave] = useState('')
	const [expensesRecord, setExpensesRecord] = useState([])

	//total Balance
	const [balance, setBalance] = useState(0)

	const [isActive, setIsActive] = useState(false)

	//search
	const [search, setSearch] = useState('')

	useEffect(() => {
		if ((incomeSource !== '' && incomeDesc !== '' && incomeAmount !== '') || (expensesName !== '' && expensesDesc !== '' && expensesAmount !== '')) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [incomeSource, incomeDesc, incomeAmount, expensesName, expensesDesc, expensesAmount])

	function addIncome(e) {
		e.preventDefault()

		fetch('https://csp3-dupaya.herokuapp.com/api/users/add-income', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				source: incomeSource,
				description: incomeDesc,
				amount: incomeAmount,
				totalIncome: totalIncome,
				userId: user.id
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data !== null) {
				alert('Saved successfully')
			} else {
				alert('Something went wrong')
			}
			setBalance(data.totalIncome)
		})
		setIncomeSource('')
		setIncomeDesc('')
		setIncomeAmount(0)
	}

	//add expenses
	function addExpenses(e) {
		e.preventDefault()

		fetch('https://csp3-dupaya.herokuapp.com/api/users/add-expenses', {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: expensesName,
				description: expensesDesc,
				amount: expensesAmount,
				userId: user.id
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data !== null) {
				console.log(data)
				alert('Saved successfully')
			} else {
				alert('Something went wrong')
			}
			setBalance(data.totalIncome - data.totalExpenditure)
		})
		setExpensesName('')
		setExpensesDesc('')
		setExpensesAmount(0)
	}

	incomeRecord.sort((a,b) => {
		if (a.totalIncome < b.totalIncome) {
			return 1
		} else if (a.totalIncome > b.totalIncome) {
			return -1
		} else {
			return 0
		}
	})

	const incomeData = incomeRecord.map(data => {
		return (
			<Card key={data._id} className="mb-3 mt-3 d-flex justify-content-between">
				<Card.Header className="text-right">Date: {moment(data.date).format('MMMM DD YYYY')}</Card.Header>
				<Card.Text className="ml-3">
					<span>Source: {data.source}</span>
					<br />
					<span>Description: {data.description}</span>
					<br />
					<Row>
						<Col xs={12} md={6}>

						</Col>

						<Col xs={12} md={6} className="text-right">
							<span className="mr-3">Amount: {data.amount}</span>
							<br />
						</Col>
					</Row>

					<Row>
						<Col xs={12} md={6}>

						</Col>

						<Col xs={12} md={6} className="text-right">
							<span className="mr-3 text-primary">Income balance: {data.totalIncome}</span>
							<br />
						</Col>
					</Row>
				</Card.Text>
			</Card>
		)
	})

	expensesRecord.sort((a,b) => {
		if (a.totalExpenses < b.totalExpenses) {
			return 1
		} else if (a.totalExpenses > b.totalExpenses) {
			return -1
		} else {
			return 0
		}
	})

	const expensesData = expensesRecord.map(data => {
		// console.log(data)
		return (
			<Card key={data._id} className="mb-3 mt-3">
				<Card.Header className="text-right">Date: {moment(data.date).format('MMMM DD YYYY')}</Card.Header>
				<Card.Text className="ml-3">
					<span>Name: {data.name}</span>
					<br />
					<span>Description: {data.description}</span>
					<br />
					<Row>
						<Col xs={12} md={6}>

						</Col>

						<Col xs={12} md={6} className="text-right">
							<span className="mr-3">Amount: {data.amount}</span>
							<br />
						</Col>
					</Row>

					<Row>
						<Col xs={12} md={6}>

						</Col>
						<Col xs={12} md={6} className="text-right">
							<span className="mr-3 text-danger">Total Expenses: {data.totalExpenses}</span>
							<br />
						</Col>
					</Row>
				</Card.Text>
			</Card>
		)
	})

	useEffect(() => {
		fetch('https://csp3-dupaya.herokuapp.com/api/users/user-details', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if (data !== null) {
				if (search !== null) {
					//search income record
					const incomeSearch = data.income.filter(data => data.source.includes(search))
					setIncomeRecord(incomeSearch)

					const expensesSearch = data.expenses.filter(data => data.name.includes(search))
					setExpensesRecord(expensesSearch)
				} else {
					setIncomeRecord(data.income)
					setExpensesRecord(data.expenses)
				}
				setBalance(data.totalIncome)
			}
		})
	}, [search, balance, incomeSource, expensesName])

	// useEffect(() => {
	// 	fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/user-details`, {
	// 		headers: {
	// 			'Content-Type': 'application/json',
	// 			Authorization: `Bearer ${localStorage.getItem('token')}`
	// 		},
	// 	})
	// 	.then(res => res.json())
	// 	.then(data => {
	// 		// console.log(data.totalIncome)

	// 		// const incomeBal = data.totalIncome

	// 		if (data !== null) {
	// 			// const tableData = []

	// 			// tableData.push(data.income)
	// 			// tableData.push(data.expenses)

	// 			// console.log(tableData)
	// 			if (search !== null) {
	// 				//search expenses record
	// 				const expensesSearch = data.expenses.filter(data => data.name.includes(search))
	// 				setExpensesRecord(expensesSearch)
	// 			} else {
	// 				setExpensesRecord(data.expenses)
	// 			}
	// 			setBalance(data.totalIncome - data.totalExpenditure)
	// 			// setRecords(tableData)
	// 		}	
	// 	})
	// }, [expensesName, expensesDesc, expensesAmount, balance, search])

	return (
		<View title={'Budget Tracker'}>

			<Container>
			<Tabs transition={false} id="transaction-tab">
				<Tab eventKey="income" title="Income">
					<Form onSubmit={(e) => addIncome(e)}>
						<Form.Group controlId="incomeSource">
							<Form.Label>Source of income</Form.Label>
							<Form.Control type="text" value={incomeSource} onChange={(e) => setIncomeSource(e.target.value)} autoComplete="off" required />
						</Form.Group>

						<Form.Group controlId="incomeDesc">
							<Form.Label>Income description</Form.Label>
							<Form.Control type="text" value={incomeDesc} onChange={(e) => setIncomeDesc(e.target.value)} autoComplete="off" required/>
						</Form.Group>

						<Form.Group controlId="incomeAmount">
							<Form.Label>Amount</Form.Label>
							<Form.Control type="number" value={incomeAmount} onChange={(e) => setIncomeAmount(e.target.value)} autoComplete="off" required />
						</Form.Group>

						{isActive === true
							?
							<Button variant="success" type="submit" block>Save</Button>
							:
							<Button variant="success" type="submit" block disabled>Save</Button>
						}
					</Form>

					<Form>
						<Form.Control type="search" autoComplete="off" value={search} onChange={(e) => setSearch(e.target.value)} />
					</Form>

					<Card>
						<Card.Text className="text-right text-success"><b>Balance: {balance}</b></Card.Text>
					</Card>
					
					{incomeData}

				</Tab>

				<Tab eventKey="expenses" title="Expenses">
					<Form onSubmit={addExpenses}>
						<Form.Group controlId="expensesName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={expensesName} onChange={(e) => setExpensesName(e.target.value)} autoComplete="off" required />
						</Form.Group>

						<Form.Group controlId="expensesDesc">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={expensesDesc} onChange={(e) => setExpensesDesc(e.target.value)} autoComplete="off" required />
						</Form.Group>

						<Form.Group controlId="expensesAmount">
							<Form.Label>Amount</Form.Label>
							<Form.Control type="number" value={expensesAmount} onChange={(e) => setExpensesAmount(e.target.value)} autoComplete="off" required />
						</Form.Group>

						{isActive === true
							?
							<Button variant="success" type="submit" block>Save</Button>
							:
							<Button variant="success" type="submit" block disabled>Save</Button>
						}
					</Form>

					<Form>
						<Form.Control type="search" autoComplete="off" value={search} onChange={(e) => setSearch(e.target.value)} />
					</Form>

					<Card>
						<Card.Text className="text-right text-success"><b>Balance: {balance}</b></Card.Text>
					</Card>

					{expensesData}
				</Tab>
			</Tabs>
			</Container>
		</View>
	)
}