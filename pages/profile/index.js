import { useState, useEffect, useContext } from 'react'
import { Container, Row, Col, Card, Form, Button } from 'react-bootstrap'
import View from '../../components/View'
import UserContext from '../../UserContext'

export default function index() {
	const { user } = useContext(UserContext)

	const [fullName, setFullName] = useState('')

	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [email, setEmail] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const [password, setPassword] = useState('')

	useEffect(() => {
		fetch('https://csp3-dupaya.herokuapp.com/api/users/user-details', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data !== null) {
				// console.log(data)
			}
			setFullName(data.firstName + ' ' + data.lastName)
			setFirstName(data.firstName)
			setLastName(data.lastName)
			setEmail(data.email)
			setMobileNo(data.mobileNo)
			setPassword(data.password)
		})
	}, [])

	console.log(password)

	return (
		<View title={`${fullName}'s' Profile`}>
			<Container>
				<Card>
					<Card.Header>Email: {email}</Card.Header>
					<Card.Body>
						<Form className="text-left">
							<Form.Group className="d-block" controlId="firstName">
							<Form.Label>First Name:</Form.Label>
								<Form.Control type="text" value={firstName} autoComplete="off" required onChange={(e) => setFirstName(e.target.value)} />
								<Button variant="primary" type="submit">Update</Button>
							</Form.Group>
						</Form>

						<Form className="text-left">
							<Form.Group className="d-block" controlId="lastName">
							<Form.Label>Last Name:</Form.Label>
								<Form.Control type="text" value={lastName} autoComplete="off" required onChange={(e) => setLastName(e.target.value)} />
								<Button variant="primary" type="submit">Update</Button>
							</Form.Group>
						</Form>

						<Form className="text-left">
							<Form.Group className="d-block" controlId="email">
							<Form.Label>Email:</Form.Label>
								<Form.Control type="email" value={email} autoComplete="off" required onChange={(e) => setEmail(e.target.value)} />
								<Button variant="primary" type="submit">Update</Button>
							</Form.Group>
						</Form>

						<Form className="text-left">
							<Form.Group className="d-block" controlId="mobileNo">
							<Form.Label>Mobile No.:</Form.Label>
								<Form.Control type="text" value={mobileNo} autoComplete="off" required maxLength={11} onChange={(e) => setMobileNo(e.target.value)} />
								<Button variant="primary" type="submit">Update</Button>
							</Form.Group>
						</Form>

						<Form className="text-left">
							<Form.Group className="d-block" controlId="password">
							<Form.Label>Password:</Form.Label>
								<Form.Control type="password" value={password} autoComplete="off" required onChange={(e) => setPassword(e.target.value)} />
								<Button variant="primary" type="submit">Update</Button>
							</Form.Group>
						</Form>
					</Card.Body>
				</Card>
			</Container>
		</View>
	)
}