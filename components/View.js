import { Container, Footer } from 'react-bootstrap'
import Head from 'next/head'

const View = ({ title, children }) => {
    return (
        <React.Fragment>
            <Head>
                <title key="title-tag">{ title }</title>
                <meta key="title-meta" name="viewport" content="initial-scale=1.0, width=device-width"/>
            </Head>
            <Container className="mt-5 pt-4 mb-5">
                { children }
            </Container>

            <footer className="font-small pt-4 mt-4 text-center">
                <h5 className="title">&#169; SWAK Budget Tracker</h5>
            </footer>
        </React.Fragment>
    )
}

export default View