import { useState, useEffect } from 'react'
import { Bar } from 'react-chartjs-2'
import {colorRandomizer} from '../helpers'

export default function ExpensesBarChart() {
	const [expensesRecords, setExpensesRecords] = useState([])

	useEffect(() => {
		fetch('https://csp3-dupaya.herokuapp.com/api/users/user-details', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data !== null) {
				setExpensesRecords(data.expenses)
			}
		})
	}, [])

	const expensesData = expensesRecords.map(data => data.totalExpenses)
	const expensesName = expensesRecords.map(data => data.name)
	const expensesDesc = expensesRecords.map(data => data.description)
	const expensesDate = expensesRecords.map(data => data.date.slice(0, 10))

	console.log(expensesData)

	const data = {
	  labels: expensesDate,
	  datasets: [
	    {
	      label: 'Expenses Bar Chart',
	      data: expensesData,
	      backgroundColor: 'rgba(255,99,132, 0.2)',
	      borderColor: 'rgba(255,99,132, 1)',
	      borderWidth: 1,
	      hoverBackgroundColor: 'rgba(255,99,132,0.2)',
	      hoverBorderColor: 'rgba(255,99,132, 1)',
	    },
	  ],
	}

	const options = {
	  scales: {
	    yAxes: [
	      {
	        ticks: {
	          beginAtZero: true,
	        },
	      },
	    ],
	  },
	}

	return (
		<Bar data={data} options={options} />
	)
}