import { useState, useEffect } from 'react'
import { Pie } from 'react-chartjs-2'
import {colorRandomizer} from '../helpers'

export default function ExpensesPieChart() {
	const [expensesRecords, setExpensesRecords] = useState([])

	useEffect(() => {
		fetch('https://csp3-dupaya.herokuapp.com/api/users/user-details', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data !== null) {
				setExpensesRecords(data.expenses)
			}
		})
	}, [])

	const expensesData = expensesRecords.map(data => data.totalExpenses)
	const expensesName = expensesRecords.map(data => data.name)
	const expensesDesc = expensesRecords.map(data => data.description)
	const expensesDate = expensesRecords.map(data => data.date.slice(0, 10))
	const bgColors = expensesRecords.map(() => `#${colorRandomizer()}`)

	const data = {
		labels: expensesDesc,
		datasets: [
			{
				data: expensesData,
				backgroundColor: bgColors,
				hoverBackgroundColor: bgColors
			}
		]
	}

	return (
		<Pie data={data} />
	)
}