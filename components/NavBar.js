import { useContext } from 'react'
import { Navbar, Nav, Container } from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../UserContext'

export default function NavBar() {
	const { user } = useContext(UserContext)
	// console.log(user)
	return (
		<Container>
			<Navbar bg="transparent" expand="lg">
				<Link href="/">
					<a className="navbar-brand">SWAK</a>
				</Link>

				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					{(user.email !== undefined || null)
					?
					<React.Fragment>
						<Nav className="mr-auto">
							<Link href="/tracker">
								<a className="nav-link" role="button">Budget Tracker</a>
							</Link>

							<Link href="/stats">
								<a className="nav-link" role="button">Stats</a>
							</Link>
						</Nav>

						<Nav className="ml-auto">
							<Link href="/profile">
								<a className="nav-link" role="button">Profile</a>
							</Link>

							<Link href="/logout">
								<a className="nav-link" role="button">Logout</a>
							</Link>
						</Nav>
					</React.Fragment>
					:
						<Nav className="ml-auto">
							<Link href="/login">
								<a className="nav-link" role="button">Login</a>
							</Link>

							<Link href="/register">
								<a className="nav-link" role="button">Register</a>
							</Link>
						</Nav>
					}
				</Navbar.Collapse>
			</Navbar>
		</Container>
	)
}