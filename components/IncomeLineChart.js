import { useState, useEffect } from 'react'
import { Line } from 'react-chartjs-2'
import {colorRandomizer} from '../helpers'

export default function IncomeLineChart() {
	const [incomeRecords, setIncomeRecords] = useState([])

	useEffect(() => {
		fetch('https://csp3-dupaya.herokuapp.com/api/users/user-details', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data !== null) {
				setIncomeRecords(data.income)
			}
		})
	}, [])

	const incomeData = incomeRecords.map(data => data.totalIncome)
	const incomeSource = incomeRecords.map(data => data.source)
	const incomeDesc = incomeRecords.map(data => data.description)
	const incomeDate = incomeRecords.map(data => data.date.slice(0, 10))
	const bgColors = incomeRecords.map(() => `#${colorRandomizer()}`)

	const data = {
		labels: incomeDesc,
		datasets: [
		   {
		     label: "Income Trend",
		     data: incomeData,
		     fill: false,
		     backgroundColor: bgColors,
		     borderColor: bgColors,
		   },
		],
	}

	return (
		<Line data={data} />
	)
}